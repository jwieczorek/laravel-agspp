<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Home Routes
|--------------------------------------------------------------------------
*/
Route::get('/', 'Web\HomeController@index');

/*
|--------------------------------------------------------------------------
| Account
|--------------------------------------------------------------------------
*/
// Get requests
Route::get('account', 'Web\AccountController@index');
Route::get('account/login', 'Web\LoginController@index');
Route::get('account/register', 'Web\RegisterController@index');
Route::get('account/profile', 'Web\AccountController@profile');

// Post requests
Route::post('account/register', 'Web\RegisterController@create');
Route::post('account/update', 'Web\RegisterController@update');

/*
|--------------------------------------------------------------------------
| Product Routes
|--------------------------------------------------------------------------
*/
Route::get('products', 'Web\ProductController@index');
Route::get('product/category/{id?}', 'Web\ProductController@category');
Route::get('product/tag/{id?}', 'Web\ProductController@tag');
Route::get('product/{id?}', 'Web\ProductController@product');

/*
|--------------------------------------------------------------------------
| Blog Routes
|--------------------------------------------------------------------------
*/
Route::get('blog', 'Web\BlogController@index');
Route::get('blog/post/{id?}', 'Web\BlogController@post');
Route::get('blog/category/{id?}', 'Web\BlogController@category');
Route::get('blog/tag/{id?}', 'Web\BlogController@category');

/*
|--------------------------------------------------------------------------
| Page Routes
|--------------------------------------------------------------------------
*/
Route::get('{page}/{subPage?}/{action?}/{arg?}', 'Web\PagesController@index');