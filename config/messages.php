<?php
return [

    /**
     * Product related messages.
     */
    'product' => [
        /**
         * Product created message.
         */
        'created' => [
            'Status' => 'Success',
            'Message' => 'Product created successfully!'
        ],

        /**
         * Product create fail message.
         */
        'createFail' => [
            'Status' => 'Fail',
            'Message' => 'Product was not created (save method did not save)!'
        ],

        /**
         * Product updated message.
         */
        'updated' => [
            'Status' => 'Success',
            'Message' => 'Product updated successfully!'
        ],

        /**
         * Product update fail message.
         */
        'updateFail' => [
            'Status' => 'Fail',
            'Message' => 'Product was not updated (save method did not save)!'
        ],

        /**
         * Product not found message.
         */
        'notFound' => [
            'Status' => 'Fail',
            'Message' => 'Product not found!'
        ]
    ],

    /**
     * User related messages.
     */
    'user' => [
        /**
         * User created message.
         */
        'created' => [
            'Status' => 'Success',
            'Message' => 'User created successfully!'
        ],

        /**
         * User create fail message.
         */
        'createFail' => [
            'Status' => 'Fail',
            'Message' => 'User was not created (save method did not save)!'
        ],

        /**
         * User updated message.
         */
        'updated' => [
            'Status' => 'Success',
            'Message' => 'User updated successfully!'
        ],

        /**
         * User update fail message.
         */
        'updateFail' => [
            'Status' => 'Fail',
            'Message' => 'User was not updated (save method did not save)!'
        ],

        /**
         * User not found message.
         */
        'notFound' => [
            'Status' => 'Fail',
            'Message' => 'User not found!'
        ]
    ],
];