<?php

return [

    /**
     * Spot price urls.
     */
    'urls' => [
        'gold' => 'http://www.freewebservicesx.com/GetGoldPrice.asmx?WSDL',
        'silver' => 'http://www.freewebservicesx.com/GetSilverPrice.asmx?WSDL',
        'platinum' => 'http://www.freewebservicesx.com/GetPlatinumPrice.asmx?WSDL',
        'palladium' => 'http://www.freewebservicesx.com/GetPalladiumPrice.asmx?WSDL',
    ],

    /**
     * Overages.
     */
    'overages' => [
        'gold' => 4.50,
        'silver' => 0.20,
        'platinum' => 4.00,
        'palladium' => 6.00
    ],

    /**
     * Compiled configs array.
     */
    'creds' => [
        'UserName' => env('SPOT_PRICE_CRED_EMAIL', 'joshuawieczorek@outlook.com'),
        'Password' => env('SPOT_PRICE_CRED_PASSWORD', 'yhwh1234'),
    ]
];