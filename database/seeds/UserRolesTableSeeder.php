<?php

use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            [
                'role_name' => 'Super Admin'
            ],
            [
                'role_name' => 'Shop Admin'
            ],
            [
                'role_name' => 'Shop Manager'
            ],
            [
                'role_name' => 'Customer Service'
            ],
            [
                'role_name' => 'Customer'
            ]
        ]);
    }
}
