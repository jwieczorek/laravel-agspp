<?php

use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->insert([
            ['name' => 'Gold'],
            ['name' => 'Silver'],
            ['name' => 'Platinum'],
            ['name' => 'Palladium']
        ]);
    }
}
