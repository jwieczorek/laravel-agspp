<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'creator_id' => '1',
            'title' => 'Awesome New Product',
            'slug' => 'awesome-new-product',
            'guide' => 'http://www.mysite/awesome-new-product',
            'price' => serialize([['Price'=>1.99]]),
            'bulk_pricing' => 0,
            'short_description' => 'This is an awesome product short desc.',
            'long_description' => 'This is an awesome product long desc.',
            'type' => 'metal',
            'access_role' => 1,
            'status' => 'published',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
