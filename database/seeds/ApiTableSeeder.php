<?php

use Illuminate\Database\Seeder;

class ApiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('api_keys')->insert([
            'user_id' => '1',
            'origin' => 'localhost',
            'username' => 'u1l_x49STRKfhHW8U0P0y2ST',
            'password' => 'u1p_8Me9Tz8HRiNB0NgYQdLnXbdWtwZaS8c723ymTyh2KgggcetMJizqNpuNIausuihk',
            'key' => 'u1k_AHp5VwqVNdeRewRshlAveX5q1vL7GOdjPD0fDpsIbBYVe1nubJ27JAoZHp5ex32dA9DoxN3JFZpQQu58WQYrLMVRB9C91k7VWSwa',
            'last_action' => ''
        ]);
    }
}
