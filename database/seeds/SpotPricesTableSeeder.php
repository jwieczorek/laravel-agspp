<?php

use Illuminate\Database\Seeder;

class SpotPricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('spot_prices')->insert([
            ['metal' => 'gold', 'ask' => 1283.84],
            ['metal' => 'silver', 'ask' => 18.34],
            ['metal' => 'platinum', 'ask' => 972.00],
            ['metal' => 'palladium', 'ask' => 792.00]
        ]);
    }
}
