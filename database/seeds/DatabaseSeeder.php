<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add default user.
        $this->call(UsersTableSeeder::class);
        // Add user roles.
        $this->call(UserRolesTableSeeder::class);
        // Assign user to roles.
        $this->call(UsersRolesTableSeeder::class);
        // Seed api keys.
        $this->call(ApiTableSeeder::class);
        // Seed shop configs.
        $this->call(ShopsConfigTableSeeder::class);
        // Seed products.
        $this->call(ProductsTableSeeder::class);
        // Seed discounts.
        $this->call(DiscountsTableSeeder::class);
        // Seed product links.
        $this->call(ProductLinksTableSeeder::class);
        // Seed product categories.
        $this->call(ProductCategoriesTableSeeder::class);
        // Seed products categories.
        $this->call(ProductsCategoriesTableSeeder::class);
        // Seed spot prices.
        $this->call(SpotPricesTableSeeder::class);
    }
}
