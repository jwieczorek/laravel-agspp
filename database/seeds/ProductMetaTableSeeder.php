<?php

use Illuminate\Database\Seeder;

class ProductMetaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_meta')->insert([
            'product_id' => 1,
            'meta_key' => 'Custom information',
            'meta_value' => 'this is the some awesome custom information'
        ]);
    }
}
