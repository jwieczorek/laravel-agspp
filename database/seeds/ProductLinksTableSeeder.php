<?php

use Illuminate\Database\Seeder;

class ProductLinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_links')->insert([
            [
                'product_id' => 1,
                'title' => 'Facebook',
                'link' => 'http://google.com'
            ],
            [
                'product_id' => 1,
                'title' => 'Twitter',
                'link' => 'http://google.com'
            ],
            [
                'product_id' => 1,
                'title' => 'LinkedIn',
                'link' => 'http://google.com'
            ],
            [
                'product_id' => 1,
                'title' => 'Google+',
                'link' => 'http://google.com'
            ]
        ]);
    }
}
