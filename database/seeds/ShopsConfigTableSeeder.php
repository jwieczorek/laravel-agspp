<?php

use Illuminate\Database\Seeder;

class ShopsConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shop_configs')->insert([
            'config_key' => 'ShopUrl',
            'config_value' => 'http://google.com'
        ]);
    }
}
