<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id');
            $table->string('title', 100);
            $table->string('slug', 100)->unique();
            $table->string('guide', 200);
            $table->longText('price');
            $table->boolean('bulk_pricing')->default(0);
            $table->longText('short_description');
            $table->longText('long_description')->nullable();
            $table->string('featured_image', 255)->nullable();
            $table->string('type');
            $table->integer('access_role')->nullable();
            $table->string('status', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
