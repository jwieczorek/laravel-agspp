<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author');
            $table->text('title');
            $table->text('slug');
            $table->text('guide');
            $table->longText('content');
            $table->longText('excerpt');
            $table->integer('parent');
            $table->text('status');
            $table->text('ping_status');
            $table->text('password');
            $table->text('type');
            $table->text('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
