<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id');
            $table->string('title', 100);
            $table->string('slug', 100);
            $table->string('guide', 200);
            $table->longText('content');
            $table->longText('excerpt');
            $table->string('featured_image', 255);
            $table->string('type');
            $table->integer('access_role');
            $table->boolean('raw')->default(1);
            $table->string('status', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
