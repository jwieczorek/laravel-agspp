<?php
/**
 * Created by PhpStorm.
 * User: joshuawieczorek
 * Date: 4/24/17
 * Time: 5:57 AM
 */

namespace App\Handlers;
use Illuminate\Http\Request;
use App\Models\User as Model;

class Users extends BaseHandler
{
    protected $user;

    /**
     * Users constructor.
     * @param Model $user
     */
    public function __construct(Model $user)
    {
        $this->user = $user;
    }

    /**
     * Get user.
     *
     * @param $user_id
     * @return mixed
     */
    private function _get_user($user_id)
    {
        if(is_numeric($user_id)):
            return $this->user->where('id', $user_id)->first();
        endif;

        return $this->user->where('username', $user_id)->first();
    }

    /**
     * Return user.
     *
     * @param $user_id
     * @return mixed
     */
    public function get($user_id)
    {
        $user = $this->_get_user($user_id);
        $user->meta = $this->generate_meta($user->user_meta);
        unset($user->user_meta);
        return $user;
    }

    public function create(Request $request)
    {

    }

    public function update(Request $request)
    {

    }
}