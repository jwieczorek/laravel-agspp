<?php

namespace App\Handlers;

use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Request;
use App\Models\ApiKey as Model;
use App\Jobs\LogSiteActivity;
use App\Jobs\UpdateSpotPrices;


class ApiKey extends BaseHandler
{
    /**
     * Create api key.
     */
    public function create()
    {

    }

    /**
     * Update api key
     */
    public function update()
    {

    }

    /**
     * Validate API request.
     *
     * @param Request $request
     * @return bool
     */
    public function validate(Request $request)
    {
        $key = Model::where([
            'username' => $request->header('Api-Login'),
            'password' => $request->header('Api-Password'),
            'key' => $request->header('Api-Key')
        ])->first();

        /**
         * If user has permission then
         * throw job to user id and log activity.
         */
        if($key && $key->user->api_access == 1) :
            Auth::loginUsingId($key->user->id);
            $injector = $this->_activity_log_data($request, $key->user->id, 'api', 200);
            dispatch(new LogSiteActivity($injector));
            return true;
        endif;

        /**
         * If user does not have permission then
         * throw job to null user and log activity.
         */
        $injector = $this->_activity_log_data($request, null, 'api', 401);
        dispatch(new LogSiteActivity($injector));
        return false;
    }
}