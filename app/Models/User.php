<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'active'
    ];

    /**
     * Get user's roles.
     *
     * @return array [user's roles]
     */
    public function roles()
    {
        return $this->belongsToMany(UserRole::class, 'users_roles', 'user_id', 'id');
    }

    /**
     * Get user's meta.
     *
     * @return array [all user's meta]
     */
    public function user_meta()
    {
        return $this->hasMany(UserMeta::class);
    }

    /**
     * Get user's api keys.
     *
     * @return array [all user's api keys]
     */
    public function api_keys()
    {
        return $this->hasMany(ApiKey::class);
    }
}
