<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    public $timestamps = false;

    protected $table = 'user_roles';

    /**
     * Get user's roles.
     */
    public function users()
    {
        $this->belongsToMany(\App\Models\User::class);
    }
}
