<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteActivity extends Model
{
    protected $table = 'site_activity';

    protected $fillable = [
        'user_id',
        'action',
        'referrer',
        'ip',
        'status'
    ];
}
