<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiKey extends Model
{
    protected $table = 'api_keys';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
