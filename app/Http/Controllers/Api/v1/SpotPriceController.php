<?php

namespace App\Http\Controllers\Api\v1;
use App\Http\Controllers\Api\ApiController;
use App\Handlers\SpotPrices;

class SpotPriceController extends ApiController
{
    /**
     * Get spot prices.
     *
     * @param SpotPrices $handler
     * @return array
     */
    public function get(SpotPrices $handler)
    {
        return $handler->get();
    }
}
