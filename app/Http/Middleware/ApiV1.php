<?php

namespace App\Http\Middleware;

use \Closure;
use \Illuminate\Http\Request;
use \Illuminate\Support\Facades\Response;
use \App\Handlers\ApiKey as Handler;

class ApiV1
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /**
         * Generate new instance of ApiKey
         */
        $ApiKeyHandler = new Handler;

        /**
         * Validate api key request.
         */
        if(false === $ApiKeyHandler->validate($request)):
            return Response::json([
                'status' => 401,
                'message' => 'Unauthorized!'
            ], 401);
        endif;

        /**
         * Return request
         */
        return $next($request);
    }
}
