<?php

namespace App\Http\Middleware;

use App\Jobs\UpdateSpotPrices;
use Closure;
use Illuminate\Foundation\Bus\DispatchesJobs;

class WebLoad
{
    use DispatchesJobs;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {




        // Return
        return $next($request);
    }
}
